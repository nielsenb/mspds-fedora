==============
 mspds-fedora
==============

Specfile for packaging the MSP debug stack (MSPDS) for the MSP430.

See the official `TI`_ page for more details.

RPMs available at the `mspds copr repository`_.

.. _TI: http://processors.wiki.ti.com/index.php/MSP_Debug_Stack
.. _mspds copr repository: https://copr.fedoraproject.org/coprs/nielsenb/mspds/
