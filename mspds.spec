%global _enable_debug_package 0
%global debug_package %{nil}

%global ti_version_number 3_13_0_001

Name:		mspds
Version:	3.13.0.1
Release:	1
Summary:	The MSP debug stack (MSPDS) for all MSP430 devices
Group:		Development/Debuggers
License:	TI-TSPA
URL:		http://www.ti.com/tool/MSPDS
Source0:	http://www.ti.com/lit/sw/slac460t/slac460y.zip
Source1:	https://raw.githubusercontent.com/joelagnel/meta-ti/master/licenses/TI-TSPA
# Udev rule
Source2:	39-msp430uif.rules
# Fix boost / std namespace issues
# https://bugs.freebsd.org/bugzilla/show_bug.cgi?id=224094
Patch0:		mspds-boostfix.patch
# Include hidapi USB
Patch1:		mspds-usbfix.patch
# Enable FPGA
Patch2:		mspds-nofpga.patch
BuildRequires:	gcc-c++
BuildRequires:	boost-devel
BuildRequires:	hidapi-devel
BuildRequires:	libusb-devel

%description
The MSP debug stack (MSPDS) for all MSP430 devices is called MSP430.DLL. It
consists of a dynamic link library as well as embedded firmware that runs on
the MSP-FET430UIF or the eZ430 emulators. It is the bridging element between
all PC software and all MSP430 microcontroller derivatives and handles tasks
such as code download, stepping through code, break point handling and so
forth. Examples that use the DLLv3 are IDEs such as Code Composer Studio (CCS),
IAR's Embedded Workbench or others. Also other TI tools like Smart RF Studio or
Elprotronic's FlashPro430 are using the MSP430.DLL.

%prep
%setup -q -c -n %{name}
%patch0 -p1
%patch1 -p1
%patch2 -p1

# Symlink hidapi
ln -s %{_includedir}/hidapi/hidapi.h ThirdParty/include

# Symlink libusb
%ifarch x86_64
ln -s %{_libdir}/libhidapi-libusb.so ThirdParty/lib64/hid-libusb.o
%else
ln -s %{_libdir}/libhidapi-libusb.so ThirdParty/lib/hid-libusb.o
%endif

# Copy udev rule
cp -a %{SOURCE1} .

# Copy the license
cp -a %{SOURCE2} .

%build
%ifarch x86_64
make CXXFLAGS="%{build_cxxflags} -fPIC" %{?_smp_mflags}
%else
make BIT32=1 CXXFLAGS="%{build_cxxflags}" %{?_smp_mflags}
%endif

%install
mkdir -p %{buildroot}%{_libdir}
mkdir -p %{buildroot}%{_sysconfdir}/udev/rules.d
install -D libmsp430.so %{buildroot}%{_libdir}/
install -D 39-msp430uif.rules %{buildroot}%{_sysconfdir}/udev/rules.d/

%files
%license TI-TSPA
%{_libdir}/libmsp430.so
/etc/udev/rules.d/39-msp430uif.rules

%changelog
* Wed Mar 27 2019 Brandon Nielsen <nielsenb@jetfuse.net> - 3.13.0.1-1
- Update to 3.13.0.1

* Thu Oct 11 2018 Brandon Nielsen <nielsenb@jetfuse.net> - 3.12.0.4-2
- Add -lboost_thread, -lboost_timer

* Fri Mar 9 2018 Brandon Nielsen <nielsenb@jetfuse.net> - 3.12.0.4-1
- Update to 3.12.0.4
- Update to TI-TSPA license, technically happened with 3.8.0.2

* Sun Oct 29 2017 Brandon Nielsen <nielsenb@jetfuse.net> - 3.10.1.0-1
- Update to 3.10.1.0
- Use passed CXXFLAGS

* Fri May 05 2017 Brandon Nielsen <nielsenb@jetfuse.net> - 3.9.2.15-1
- Promote udev rule to 39 (40 works as well) for programming as user

* Fri Jun 24 2016 Brandon Nielsen <nielsenb@jetfuse.net> - 3.5.1.1-4
- Promote udev rule to 39 (40 works as well) for programming as user

* Fri Jun 24 2016 Brandon Nielsen <nielsenb@jetfuse.net> - 3.5.1.1-3
- Explicitly require gcc-c++ as per Fedora packaging guidelines

* Wed Jun 15 2016 Brandon Nielsen <nielsenb@jetfuse.net> 3.5.1.1-2
- Explicitly include <mutex> for compiling with GCC 6
- Explicitly include <fstream> for compiling with GCC 6

* Tue Oct 27 2015 Brandon Nielsen <nielsenb@jetfuse.net> 3.5.1.1
- Update to 3.5.1.1

* Thu Aug 13 2015 Brandon Nielsen <nielsenb@jetfuse.net> 3.5.1.0
- Update to 3.5.1.0

* Tue Jun 2 2015 Brandon Nielsen <nielsenb@jetfuse.net> 3.5.0.1
- Update to 3.5.0.1

* Sat Jan 31 2015 Brandon Nielsen <nielsenb@jetfuse.net> 3.4.3.4
- Initial version
